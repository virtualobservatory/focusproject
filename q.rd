<!-- A template for a simple DaCHS SIAP (image search) service.
To fill it out, search and replace %.*% 

Note that this doesn't expose all features of DaCHS.  For advanced
projects, you'll still have to read documentation... -->


<resource schema="focusProject">
  <meta name="creationDate">2023-02-02T10:19:08Z</meta>

  <meta name="title">Focus Project</meta>
  <meta name="description">
    A nice decsription goes here
  </meta>
  <!-- Take keywords from 
    http://www.ivoa.net/rdf/uat
    if at all possible -->
  <meta name="subject">%keywords; repeat the element as needed%</meta>

  <meta name="creator">%authors in the format Last, F.I; Next, A.%</meta>
  <meta name="instrument">%telescope or detector or code used%</meta>
  <meta name="facility">%observatory/probe at which the data was taken%</meta>

  <meta name="source">%ideally, a bibcode%</meta>
  <meta name="contentLevel">Research</meta>
  <meta name="type">Catalog</meta>  <!-- or Archive, Survey, Simulation -->

  <!-- Waveband is of Radio, Millimeter, 
      Infrared, Optical, UV, EUV, X-ray, Gamma-ray, can be repeated -->
  <meta name="coverage.waveband">%word from controlled vocabulary%</meta>

  <table id="main" onDisk="True" mixin="//scs#q3cindex" adql="True">

    <column name="id" type="text"
      ucd="meta.id;meta.main"
      tablehead="Id"
      description="Main identifier for this object."
      verbLevel="1"/>
    <column name="ra" type="double precision"
      unit="deg" ucd="pos.eq.ra;meta.main"
      tablehead="RA"
      description="ICRS right ascension for this object."
      verbLevel="1"/>
    <column name="dec" type="double precision"
      unit="deg" ucd="pos.eq.dec;meta.main"
      tablehead="Dec"
      description="ICRS declination for this object."
      verbLevel="1"/>
     
    
    <column description="Data product type" name="type" required="True" type="text" ucd="meta.id"/>
    <column description="Format" name="access_format" required="True" type="text" utype="Access.format"/>
    <column description="Access size" name="access_size" required="True" type="bigint" />
    <column description="Access URL" name="access_url" required="True" type="text"  utype="Access.reference"/>
    <column description="Exposure time" name="t_exptime" tablehead="Exposure time" required="True" type="real"/>
    <column description="Central frequency" name="freq_ref" tablehead="Central frequency" required="True" type="real"/>
    <column description="Frequency resolution" name="freq_resolution" tablehead="Central frequency" required="True" type="real"/>
    <column description="Time resolution" name="t_resolution" tablehead="Time resolution" required="True" type="real"/>
    <column description="Bandwidth" name="freq_bandwidth" tablehead="Bandwidth" required="True" type="real"/>
    <column description="Sky footprint" name="s_region" type="text" tablehead="Sky footprint" />
    <column description="FHWM of PSF" name="s_resolution_min" type="double precision"/>
    
    <column description="Activity" name="activity" type="text" />
  </table>
  
  <table id="activity" onDisk="True" adql="True">
    <column name="id" type="text"
      ucd="meta.id;meta.main"
      tablehead="Id"
      description="Main identifier for this object."
      verbLevel="1"/>
    <column name="type" type="text"
      ucd="meta.id"
      tablehead="Type"
      description="Type of activity"
      verbLevel="1"/>
    <column name="project" type="text" ucd="meta.id" tablehead="Project" description="Project code"/>
    <column name="collection" type="text" ucd="meta.id" tablehead="Collection"/>
  </table>
  <table id="secondary" onDisk="True" adql="True">
  
    <column name="activity" description="Activity" type="text" />
    <column description="Data product type" name="Type" required="True" type="text" ucd="meta.id"/>
    <column description="Format" name="access_format" required="True" type="text" utype="Access.format"/>
    
    <column description="Access URL" name="access_url" required="True" type="text"  utype="Access.reference"/>
    
  </table>

  <coverage>
    <updater sourceTable="main"/>
  </coverage>

  <!-- if you have data that is continually added to, consider using
    updating="True" and an ignorePattern here; see also howDoI.html,
    incremental updating -->
  <data id="import">
    <sources pattern="%resdir-relative pattern, like data/*.fits%"/>

    <!-- the fitsProdGrammar should do it for whenever you have
    halfway usable FITS files.  If they're not halfway usable,
    consider running a processor to fix them first – you'll hand
    them out to users, and when DaCHS can't deal with them, chances
    are their clients can't either -->
    <sources pattern="sql/main.yml"/>
        <property key="previewDir">previews</property>

        <customGrammar module="res/sqlgrammar">

        </customGrammar>

    <make table="main">
      <rowmaker>
        <map name="id">@id</map>
        <map name="ra">@ra</map>
        <map name="dec">@dec</map>
        <map name="type">@type</map>
        <map name="access_format">@access_format</map>
        <map name="access_url">@access_url</map>
        <map name="access_size">@access_size</map>
        <map name="t_exptime">@t_exptime</map>
        <map name="freq_ref">@freq_ref</map>
        <map name="freq_resolution">@freq_resolution</map>
        <map name="t_resolution">@t_resolution</map>
        <map name="freq_bandwidth">@freq_bandwidth</map>
        <map name="s_region">@s_region</map>
        <map name="s_resolution_min">@s_resolution_min</map>
        <map name="activity">@activity</map>  
      </rowmaker>
    </make>
  </data>

</resource>
