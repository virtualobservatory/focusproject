from gavo.grammars.customgrammar import CustomRowIterator
import yaml
from sqlalchemy import create_engine, text
from argparse import ArgumentParser


def parse_args():
    parser = ArgumentParser(description='SQL grammar for DACHS')
    parser.add_argument('operation', choices=['print', 'suggestMeta'])
    parser.add_argument(
        'sqlFile', help='YAML file containing the query to execute')
    return parser.parse_args()


def read_sql(conffile_path):
    with open(conffile_path, 'r') as fin:
        config = yaml.load(fin, Loader=yaml.SafeLoader)
        engine = create_engine(config['connection'])
        with engine.connect() as conn:
            result = conn.execute(config['sql'])

            return result
    return None


class RowIterator(CustomRowIterator):
    def __init__(self, grammar, sourceToken, sourceRow=None):
        CustomRowIterator.__init__(self, grammar, sourceToken, sourceRow)

    def _iterRows(self):
        result = read_sql(self.sourceToken)
        for row in result:
            row_dict = {key: value for key, value in zip(result.keys(), row)}
            yield row_dict

    def suggestRowMakerContent(self):
        one_item = next(self._iterRows())

        for key in one_item.keys():
            print('<map name="%(key)s">@%(key)s</map>' % {'key': key})


if __name__ == '__main__':
    import sys
    args = parse_args()
    iterator = RowIterator('', args.sqlFile, '')
    if args.operation == 'print':
        for k in iterator._iterRows():
            print(k)
    elif args.operation == 'suggestMeta':
        iterator.suggestRowMakerContent()
