# FocusProject resource definition for the VirtualObservaotry

In this repository the resource description for publishing the Focus Project
intermediary products into the virtual observatory (through GAVO DACHS) are 
contained.


In particular the resource definition file is at the path `q.rd`. Please
modify that file to publish more services or to restructure the format of the 
tables.

Futhermore, the python utilities to import the data are available in the `res`
directory. Such utilities are meant to be used by the resource description
file and called directly by dachs during the import.

To simplify testing and development a CI/CD pipeline is configured and the 
necessary files for adding tests are stored in the `test` directory.

Note that another directory is present and it is called `sql` in that directory
the configuration files fro the grammars are kept in yaml format.


## Schema of the resource
The current tables schema created is 
![Database schema](docs/schema.png "Database schema")

### Recreate the database schema image
To recreated it use the utilities stored in `utils/plot_schema.py`

```bash
utils/plot_schema.py [resource_description] [name_of_the_file_without_extension]
```

The command above will create a `[name_of_the_file_without_extension].png` file.
