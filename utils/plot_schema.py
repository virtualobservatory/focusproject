#!/usr/bin/env python3

import graphviz
from argparse import ArgumentParser
import xml.etree.ElementTree as ET


def parse_args():
    parser = ArgumentParser(
        description="Generate database table diagram from resource description")
    parser.add_argument("rd", help="Resource description file")
    parser.add_argument("diagram", help="Where to save the diagram")

    return parser.parse_args()


def table_to_dict(table):
    return {column.attrib["name"]: column.attrib["type"] for column in table}


def read_schema_tables(filein):
    parsed = ET.parse(filein)
    resource = parsed.getroot()
    schema = resource.attrib["schema"]
    tables = parsed.findall("table")

    return schema, {table.attrib["id"]: table_to_dict(table) for table in tables}


def tables_dict_to_graph(schema, tables):
    dot = graphviz.Digraph(comment=f"{schema}", format="png")
    dot.attr("node", shape="record", style="filled",
             colorscheme="set312", fontsize="12")

    for t_number, (tname, cols) in enumerate(tables.items()):

        columns = "".join([
            f"<tr><td><b>{name}</b>: </td><td>{type}</td></tr>" for name, type in cols.items()])
        label = f"""<<table border="0" cellborder="2" cellpadding="4">
                    <tr><td><b>{tname}</b></td></tr>
                    <tr><td><table border="0" cellborder="0" cellspacing="0">{columns}</table></td></tr>
                    </table>>""".replace("\n", "")
        dot.node(name=tname, label=label, fillcolor=str(t_number + 1))
    return dot


def main():
    args = parse_args()
    schema, tables = read_schema_tables(args.rd)
    dot = tables_dict_to_graph(schema, tables)
    dot.render(args.diagram, cleanup=True)


if __name__ == "__main__":
    main()
