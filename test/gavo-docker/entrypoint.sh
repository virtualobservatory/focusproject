#!/bin/bash

PGPASSWORD=$POSTGRES_PASSWORD
IS_INIT=$(psql -h $POSTGRES_HOST -U $POSTGRES_USER -d $POSTGRES_DB -c "select exists(select * from pg_tables where schemaname='dc' and tablename='tablemeta')" -t -A)

if [ "$IS_INIT" == "t" ]; then
    echo "GAVO database already initialized: skipping initialization..."
else
    echo "Initializing gavo database"
    gavo init -d "host=$POSTGRES_HOST user=$POSTGRES_USER dbname=$POSTGRES_DB password=$POSTGRES_PASSWORD"
fi

/bin/bash