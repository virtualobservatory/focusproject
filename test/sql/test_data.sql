CREATE DATABASE lta;
\c lta;

CREATE USER test WITH PASSWORD 'test';

GRANT ALL PRIVILEGES ON DATABASE lta to test;
ALTER DATABASE lta OWNER TO test;

DROP TABLE main;
CREATE TABLE main (id TEXT, ra DOUBLE PRECISION, dec DOUBLE PRECISION, type TEXT, access_format TEXT, access_size BIGINT, access_url TEXT,t_exptime REAL,
freq_ref REAL,freq_resolution REAL, t_resolution REAL,freq_bandwidth REAL,s_region TEXT,s_resolution_min DOUBLE PRECISION, activity TEXT);

INSERT INTO main
 (id, ra, dec, type, access_format, access_size, access_url,t_exptime,freq_ref,freq_resolution, t_resolution,freq_bandwidth,s_region,s_resolution_min, activity) 
VALUES ('s001',155.5,45.6, 'visibility', 'tar', 1000,  'https://someurl/s001_MS.tar', 5, 15, 1,300, 450, 'CIRCLE()', 35, 'still'),
       ('s002',145.5,55.6, 'visibility','tar', 1000,  'https://someurl/s002_MS.tar.gz', 5, 15, 1,300, 450, 'CIRCLE()', 35, 'freckles'),
       ('s001_fits',95.5,145.6, 'image/fits', 'fits', 1000, 'https://someurl/s001.fits', 5, 15, 1,300, 450, 'CIRCLE()', 35, 'freckles'),
       ('s004',45.5,85.6, 'calibration/table', 'h5', 1000,  'https://someurl/caltable.h5', 5, 15, 1,300, 450, 'CIRCLE()', 35, 'still'),
       ('s002_fits',105.5,55.6, 'image/fits', 'fits', 1000,  'https://someurl/s002.fits', 5, 15, 1,300, 450, 'CIRCLE()', 35, 'freckles');

DROP TABLE activity;
CREATE TABLE activity(id TEXT, type TEXT, project TEXT, collection TEXT);

INSERT INTO activity (id, type, project, collection) VALUES
('A001', 'still','00005S3E','5DE4'),
('A002', 'still','000183E','5DE4'),
('A003', 'rapid movements','00234S3f','5xXE4'),
('A004', 'still','004805S3E','6aE4'),
('A005', 'freckles','00005S3E','6DE3');


DROP TABLE secondary;
CREATE TABLE secondary(activity TEXT, type TEXT, access_format TEXT, access_url TEXT, access_size BIGINT);

INSERT INTO secondary (activity, type, access_format, access_url, access_size) 
VALUES( 'still', 'raster','readable', 'www.boring.com', 10000),
( 'rapid movements', 'raster','readable', 'www.ambigous.com', 10000),
( 'still', 'raster','readable', 'www.hohoo.com', 20000),
( 'rapid movements', 'raster','readable', 'www.ambigous.com', 30000),
( 'still', 'raster','readable', 'www.sentinel.com', 1000);
GRANT ALL PRIVILEGES ON TABLE main TO test;
GRANT ALL PRIVILEGES ON TABLE secondary TO test;
GRANT ALL PRIVILEGES ON TABLE activity TO test;
